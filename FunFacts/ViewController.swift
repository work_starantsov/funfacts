//
//  ViewController.swift
//  FunFacts
//
//  Created by Nazar Starantsov on 04/08/2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

import UIKit
import GameKit

class ViewController: UIViewController {
    @IBOutlet weak var funFactLbl: UILabel!
    @IBOutlet weak var buttonChange: UIButton!
    
    let factProvider = FactProvider()

    override func viewDidLoad() {
        super.viewDidLoad()
        funFactLbl.text = factProvider.randomFact()
     }

    @IBAction func showFact() {
        let randomColor = BackgroundColorProvider().randomColor()
        
        funFactLbl.text = factProvider.randomFact()
        buttonChange.tintColor = randomColor
        view.backgroundColor = randomColor
    }
}

